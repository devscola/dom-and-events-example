class Slideshow {
  constructor(initialPageIndex, pages) {
    this.currentPageIdx = initialPageIndex
    this.pages = pages
    this.previousButton = document.querySelector('#previous')
    this.nextButton = document.querySelector('#next')
    this.bulletList = document.querySelector('#bulletList')

    this.previousButton.addEventListener('click', () => this.navigatePrevious())
    this.nextButton.addEventListener('click', event => this.navigateNext(event))
  }

  renderPage(pageIdx = this.currentPageIdx) {
    let currentPage = this.pages[pageIdx]

    const headerElement = document.querySelector('h1')
    headerElement.innerHTML = currentPage.title || ''

    const contentElement = document.querySelector('#content')
    contentElement.innerHTML = currentPage.content || ''

    this.bulletList.innerHTML = ''

    if (currentPage.bullets) {
      this.addBullets(currentPage.bullets)
    }

    let customEventSampleElement = document.querySelector('#custom_event_sample')
    if (customEventSampleElement) {
      const sampleContent = this.customEventContent()
      customEventSampleElement.appendChild(sampleContent)
    }
    this.renderButtons()
  }

  navigateNext(event) {
    // console.log(event)
    // console.log(`Navigating to: ${event.target.innerHTML}`)
    this.currentPageIdx += 1
    this.renderButtons()
    this.renderPage()
  }

  navigatePrevious() {
    this.currentPageIdx -= 1
    this.renderButtons()
    this.renderPage()
  }

  renderButtons() {
    this.showPreviousButton()
    this.showNextButton()

    if (this.isFirstPage()) this.hidePreviousButton()
    if (this.isLastPage()) this.hideNextButton()
  }

  addBullets(bullets) {
    bullets.forEach(bullet => {
      const newLiElement = document.createElement('li')
      newLiElement.innerHTML = bullet
      this.bulletList.appendChild(newLiElement)
    })
  }

  showNextButton() {
    this.nextButton.classList.remove('hidden')
  }

  hideNextButton() {
    this.nextButton.classList.add('hidden')
  }

  showPreviousButton() {
    this.previousButton.classList.remove('hidden')
  }

  hidePreviousButton() {
    this.previousButton.classList.add('hidden')
  }

  isFirstPage() {
    return this.currentPageIdx === 0
  }

  isLastPage() {
    const pagesLastIndex = pages.length - 1

    return this.currentPageIdx === pagesLastIndex
  }

  customEventContent() {
    let contentDivElement = document.createElement('div')
    contentDivElement.setAttribute('id', 'content')

    let labelElement = document.createElement('span')
    labelElement.style['margin-right'] = '4px'
    labelElement.innerHTML = '<b>Segundos de retardo: </b>'

    let inputElement = document.createElement('input')
    inputElement.setAttribute('placeholder', 'segundos')

    let buttonElement = document.createElement('button')
    buttonElement.innerHTML = "Ejecutar"

    contentDivElement.appendChild(labelElement)
    contentDivElement.appendChild(inputElement)
    contentDivElement.appendChild(buttonElement)

    buttonElement.addEventListener('click', (event) => {
      event.stopPropagation()
      this.delayedFunction(inputElement.value)
    })
    document.addEventListener('process.finished', (event) => {
      alert(`Teminó el proceso en ${event.detail} segundos`)
    })

    return contentDivElement
  }

  delayedFunction(seconds) {
    const miliseconds = seconds * 1000
    const event = new CustomEvent('process.finished', {
      detail: seconds
    })

    setTimeout(() => {
      document.dispatchEvent(event)
    }, miliseconds)
  }
}
