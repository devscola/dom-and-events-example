// Hasta que no se recibe el evento del DOM 'onload', no se pueden seleccionar
// elementos de la página

// DOM
// Eventos definición
// - Registro addEventListener
// - Subscripción
// Eventos DOM
// Scope de eventos
// Eventos predefinidos
// Eventos Custom

const pages = [
  {
    title: 'DOM y los eventos en javascript',
    content: 'Conceptos básicos para la creación dinámica de documentos HTML',
  }, {
    title: '¿Que es el DOM?',
    content: 'Son unas siglas que significan Document Object Model',
    bullets: [
      'Es un objeto global de javascript',
      'Implementa la estructura de nodos de un documento html',
      'Tiene métodos para realizar operaciones sobre los nodos (HTML elements)'
    ]
  }, {
    title: '¿Para que sirve?',
    bullets: [
      'Permite mostrar contenido dinámico en una página',
      'Se pueden utilizar datos de otras fuentes externas',
      'Mediante otras técnicas podemos actualizar la información sin recargar',
      'Podemos contolar la visualización con logica de javascript'
    ]
  }, {
    title: '¿Que operaciones se pueden realizar?',
    bullets: [
      'Seleccionar un nodo y acceder a su contenido',
      'Crear nuevos nodos HTML (tags) "createElement"',
      'Añadir nuevos nodos dentro de los ya existentes "appendChild',
      'Eliminar nodos existentes "revoveChildren"',
      'Operar con sus atributos (get, set, remove)',
      'Modificar su contenido "propiedad innerHTML"',
      'Modificar su estilo CSS',
      'Operar con las clases CSS'
    ]
  }, {
    title: 'La selección de nodos',
    content: 'Para poder realizar acciones sobre un nodo o nodos, primero tenemos que seleccionarlos',
    bullets: [
      'Seleccion simple o múltiple "querySelector y querySelectorAll"',
      'Por tagname',
      'Por css classname',
      'Por id'
    ]
  }, {
    title: 'Los eventos',
    bullets: [
      'Sucesos que ocurren en un momento determinado',
      'Eventos del objeto window',
      'Eventos definidas por el DOM en algún elemento de la página',
      'Eventos custom definidos según necesidad'
    ]
  }, {
    title: '¿Para que sirven?',
    bullets: [
      'Permiten la interacción del usuario con la página',
      'Nos permiten interactuar con el estado del navegador',
      'Podemos crear cualquier tipo de interacción que necesitemos',
      'Podemos desencadenar acciones en respuesta a estos sucesos'
    ]
  }, {
    title: '¿Cómo se usan los eventos predefinidos?',
    bullets: [
      'Tienen un derminado scope (window, document, htmlElement)',
      'Registramos un observador "listener" en un determinado scope para un mensaje concreto',
      'Añadimos una acción al observador para desencadenarse al recibir el mensaje',
      'En la acción podemos recibir el parámetro del evento'
    ]
  }, {
    title: 'Un ejemplo de evento custom',
    content: '<div id="custom_event_sample"></div>'
  }
]

const resources = [
  'https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API'
]
